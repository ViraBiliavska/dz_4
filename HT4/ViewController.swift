//
//  ViewController.swift
//  HT4
//
//  Created by Mac on 20.07.2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        print("Task 1\n")
        myNameSymbolsNumber(myName: "Вера")
        
        print("\nTask 2\n")
        checkEnd(myPatronymic: "Владимировна")
        
        print("\nTask 3\n")
        separation(foolName: "ВераБелявская")
        
        print("\nTask 4\n")
        mirror(word: "Вера")
        
        print("\nTask 5\n")
        format(text: "абвгдежзил")
        
//        print("\nTask 6\n")
//        checkPassword(password: "gkTkj8+hg5%SL1*hf")
        
        print("\nTask 7\n")
        sortArrey()
        
        print("\nTask Boxes\n")
        drawBox()
        drawBoxesFigure(boxWidth: 15, boxHeight: 15, quantity: 5)
        
    }
    
    func myNameSymbolsNumber(myName: String) {
        print("Количество символов в имени \(myName) - \(myName.count)")
    }
    
    func checkEnd(myPatronymic: String) {
        if myPatronymic.hasSuffix("на") {
            print("В отчестве \(myPatronymic) окончание -на")
        } else if myPatronymic.hasSuffix("ич") {
            print("В отчестве \(myPatronymic) окончание -ич")
        }
    }
    //        for character in myName { // выбрать только заглавные
    //            if character >= "А" && character <= "Я"{
    //                print(character)
    //            }
    //        }
    
    func separation(foolName: String) {
        let indexName = foolName.index(of: "Б") ?? foolName.endIndex
        let name = foolName[..<indexName]
        let surname = foolName[indexName...]
        let firstName = String(name)
        let secondName = String(surname)
        print(firstName)
        print(secondName)
        print(firstName + " " + secondName)
    }
    
    func mirror(word: String) {
        var newWord = " "
        for character in word {
            var letter = "\(character)"
            newWord = letter + newWord
        }
        print(word, "-", newWord)
    }
    
    func format(text: String) {
        var newText = ""
        let a = text.count % 3
        var quantity = 0
        var quantityTotal = 0
        var delenie = 0
        for character in text {
            let letter = "\(character)"
            newText = newText + letter
            quantity += 1
            quantityTotal += 1
            delenie = quantity % 3
            if quantityTotal == text.count {
                break
            }
            if a == 1 && quantity == 1 {
                newText += ","
                quantity = 3
            } else if a == 2 && quantity == 2 {
                newText += ","
                quantity = 3
            } else if delenie == 0 {
                newText += ","
            }
        }
        print(text, "->", newText)
    }
    
    func sortArrey() {
        var arrey = [2, 1, 2, 3, 7, 1, 3, 3, 3, 3, 4, 4, 4, 4]
        let repeats = arrey.count
        print("Массив \(arrey)")
        for i in 0..<repeats {
            for j in 0..<repeats - 1 - i {
                let max = arrey[j]
                if arrey[j] > arrey[j + 1] {
                    let maxLast = arrey[j + 1]
                    arrey[j + 1] = max
                    arrey[j] = maxLast
                }
            }
        }
        print("Отсортированный массив \(arrey)")
        
//            for k in 0..<repeats {
//            if arrey[k] == arrey[k + 1] {
//
//                arrey.remove(at: k)
////
//                print(k, arrey)
//
//                if arrey[k + 1] == arrey.last {
//                    break
//                }
//
//            }
//    print("Массив без дубликатов \(arrey)")
//    }
    }
    
    func drawBox () {
        let numberOfBoxes = 3
        var xDistance = 10
        for _ in 0..<numberOfBoxes {
            let rect = CGRect.init(x: xDistance, y: 100, width: 50, height: 50)
            let box = UIView(frame: rect)
            let color = UIColor(red: 47/255, green: 238/255, blue: 167/255, alpha: 1)
            box.backgroundColor = color
            view.addSubview(box)
            xDistance += 75
        }
    }
    
    func drawBoxesFigure(boxWidth: Int, boxHeight: Int, quantity: Int) {
        var numberOfBoxes = quantity
        var xDistance = boxWidth * 2
        var yDistance = boxHeight * 20
        let xSpace = boxWidth * 2
//        while numberOfBoxes > 0 {
        for _ in 0..<numberOfBoxes {
            for i in 0..<numberOfBoxes {
                let rect = CGRect.init(x: xDistance, y: yDistance, width: boxWidth, height: boxHeight)
                let box = UIView(frame: rect)
                let color = UIColor(red: 47/255, green: 238/255, blue: 167/255, alpha: 1)
                box.backgroundColor = color
                view.addSubview(box)
                xDistance += xSpace
                if i == numberOfBoxes - 1 {
                    yDistance = yDistance - boxHeight * 2
//                xDistance = xDistance - xSpace * numberOfBoxes // для построения в столбик по правому краю
                    xDistance = xDistance - xSpace * numberOfBoxes + xSpace / 2
                    numberOfBoxes = numberOfBoxes - 1
                }
            }
        }
    }
}
    

    
        
      


    
    





























    
    
    


